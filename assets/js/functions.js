var func = {}
func.runCountDown = function () {
    setInterval(function () {
        func.loadCountDownTime();
    }, 1000);
};
func.loadCountDownTime = function () {
    $('.countdownTime').each(function () {
        // var endTime = $(this).attr('data');
        var endTime = new Date("7/30/2017").getTime();
        var days = 24*60*60, hours = 60 * 60, minutes = 60;
        var left = Math.floor((endTime - (new Date())) / 1000);
// trừ số giây sai lệch
        if (left < 0) {
            left = 0;
        }
        if (left === 0) {
            $(this).html('Expired Products');
            return;
        }
        d = Math.floor(left / days);
        left -= d * days;
        h = Math.floor(left / hours);
        left -= h * hours;
        m = Math.floor(left / minutes);
        left -= m * minutes;
        s = left;
        var title = "Còn lại ";
        var titleDay = "ngày ";
        var titleHouse = "giờ ";
        var titleMin = "phút ";
        var titleSecond = "giây";
        // $(this).html(title + ' ' + d + ' ' + titleDay + ' ' + h + ' ' + titleHouse + ' ' + m + ' ' + titleMin + ' ' + s + ' ' + titleSecond);
        $(this).html('<div class="col-md-2"> <div class="count">'+d+'</div><h2>Ngày</h2> </div> <div class="col-md-2"> <div class="count">'+h+'</div><h2>Giờ</h2> </div> <div class="col-md-2"> <div class="count">'+m+'</div><h2>Phút</h2> </div> <div class="col-md-2"> <div class="count">'+s+'</div><h2>Giây</h2> </div>');




    });
};